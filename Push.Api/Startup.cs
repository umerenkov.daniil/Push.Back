﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Push.Core.Configurations;
using Push.Core.Configurations.Settings;
using Push.Core.EF;
using Push.Core.Services;
using Push.Core.SignalR;
using Push.Domain.Users;
using Swashbuckle.AspNetCore.Swagger;

namespace Push.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
			services.Configure<VapidSettings>(Configuration.GetSection("Vapid"));
			services.Configure<DbConfiguration>(Configuration.GetSection("ConnectionStrings"));
			services.Configure<JwtSettings>(Configuration.GetSection("JWTAuth"));
			services.Configure<CommonSettings>(Configuration.GetSection("CommonSettings"));

			services.AddSingleton<WebPushService>();
			services.AddSingleton<JwtService>();

			services.AddTransient<Services.UserService>();
			services.AddTransient<Core.Services.UserService>();

			services.AddTransient<Services.ChatService>();
			services.AddTransient<Core.Services.ChatService>();

			services.AddTransient<Services.PushService>();
			services.AddTransient<Core.Services.PushService>();

	        services.AddTransient<Services.TestService>();
	        services.AddTransient<Core.Services.TestService>();



			var connection = Configuration.GetConnectionString("DefaultConnection");
			services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connection));

			services.AddTransient<DbAccessor<AppDbContext>>();

			services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
			{
				builder
					.AllowAnyMethod()
					.AllowAnyHeader()
					.AllowAnyOrigin()
					.AllowCredentials();
			}));

			var identityBuilder = services.AddIdentity<User, Role>(o =>
			{
				o.Password.RequireDigit = false;
				o.Password.RequireLowercase = false;
				o.Password.RequireUppercase = false;
				o.Password.RequireNonAlphanumeric = false;
				o.Password.RequiredLength = 0;
			});

			identityBuilder = new IdentityBuilder(identityBuilder.UserType, typeof(Role), identityBuilder.Services);
			identityBuilder.AddEntityFrameworkStores<AppDbContext>().AddDefaultTokenProviders();

			services.AddAutoMapper();

			var key = Encoding.ASCII.GetBytes("00330755-0CA5-490D-9516-D8BDEF622636");
	        services.AddAuthentication(x =>
		        {
			        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
			        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
		        })
		        .AddJwtBearer(x =>
		        {
			        x.RequireHttpsMetadata = false;
			        x.SaveToken = true;
			        x.TokenValidationParameters = new TokenValidationParameters
			        {
				        ValidateIssuerSigningKey = true,
				        IssuerSigningKey = new SymmetricSecurityKey(key),
				        ValidateIssuer = false,
				        ValidateAudience = false
			        };
		        });

			services.AddSignalR();

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
			});

			services.AddMvc()
			.AddJsonOptions(options =>
				{ 
					options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Unspecified;
				})
			.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
		}

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
			if (env.IsDevelopment())
	        {
		        app.UseDeveloperExceptionPage();
	        }
	        else
	        {
		        app.UseHsts();
	        }

	        loggerFactory.AddConsole();

	        app.UseCors("CorsPolicy");
	        app.UseSignalR(routes =>
	        {
		        routes.MapHub<ChatHub>("/chat");
	        });

	        app.UseSwagger();

	        app.UseSwaggerUI(c =>
	        {
		        c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
	        });

	        app.UseAuthentication();
	        app.UseMvc();
		}
    }
}
