﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Push.Api.Controllers.Base;
using Push.Api.Models.Response;
using Push.Api.Models.Users;
using Push.Api.Services;
using Push.Domain.Users;

namespace Push.Api.Controllers
{
	[Route("api/[controller]")]
	public class UserController : BaseController
	{
		private readonly UserService _userService;

		public UserController(UserService userService)
		{
			_userService = userService;
		}

	    [AllowAnonymous]
	    [HttpPost("[action]")]
	    public async Task<Response<bool>> Register([FromBody] RegistrationRequest request)
	    {
		    try
		    {
			    var result = await _userService.Register(request);
			    return Ack(result);
		    }
		    catch (Exception ex)
		    {
			    return Nack<bool>(ex);
		    }
	    }

		[AllowAnonymous]
		[HttpPost("[action]")]
		public async Task<Response<string>> Login([FromBody] LoginRequest request)
		{
			try
			{
				var result = await _userService.Login(request);
				return Ack(result);
			}
			catch (Exception ex)
			{
				return Nack<string>(ex);
			}
		}

		[Authorize]
		[HttpGet("[action]")]
		public async Task<Response<UserDto>> GetCurrentUser()
		{
			try
			{
				var result = await _userService.GetCurrentUser();
				return Ack(result);
			}
			catch (Exception ex)
			{
				return Nack<UserDto>(ex);
			}
		}

		[Authorize]
		[HttpGet("[action]")]
		public async Task<Response<List<UserDto>>> GetAllUsers()
		{
			try
			{
				var users = await _userService.GetAllUsers();
				return Ack(users);
			}
			catch (Exception ex)
			{
				return Nack<List<UserDto>>(ex);
			}
		}
	}
}