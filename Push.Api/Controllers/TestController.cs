﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Push.Api.Controllers.Base;
using Push.Api.Models.Response;
using Push.Api.Services;

namespace Push.Api.Controllers
{
	[Route("api/[controller]")]
    public class TestController : BaseController
	{
		private readonly TestService _testService;

	    public TestController(TestService testService)
	    {
		    _testService = testService;
	    }

	    [AllowAnonymous]
	    [HttpPost("[action]")]
	    public Response<DateTime> SaveDateTime([FromBody] DateTime date)
	    {
		    try
		    {
			    var test = DateTime.Now;

			    return Ack(_testService.SaveDateTime(date));
		    }
		    catch (Exception ex)
		    {
			    return Nack<DateTime>(ex);
		    }
	    }

		[AllowAnonymous]
		[HttpGet("[action]")]
		public Response<DateTime> LastInserted()
		{
			try
			{
				var date = _testService.LastInserted();
				var test = DateTime.SpecifyKind(date, DateTimeKind.Local);
				var test3 = DateTime.Now;
				var test2 = test.ToUniversalTime();
				return Ack(test2);
			}
			catch (Exception ex)
			{
				return Nack<DateTime>(ex);
			}
		}

		[AllowAnonymous]
		[HttpPost("[action]")]
		public Response<string> TestUnspecified([FromBody] DateTime date)
		{
			return Ack("test");
		}
	}
}
