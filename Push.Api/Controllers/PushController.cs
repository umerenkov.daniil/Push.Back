﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Push.Api.Controllers.Base;
using Push.Api.Models;
using Push.Api.Models.Response;
using Push.Api.Services;

namespace Push.Api.Controllers
{
	[Route("api/[controller]")]
	public class PushController : BaseController
	{
		private readonly PushService _pushService;

		public PushController(PushService pushService)
		{
			_pushService = pushService;
		}

		[Authorize]
		[HttpPost("[action]")]
		public Response<object> SaveSubscription([FromBody] PushSubscriptionDto sub)
		{
			try
			{
				_pushService.SaveEndpoint(sub);
				return Ack<object>(null);
			}
			catch (Exception ex)
			{
				return Nack<object>(ex);
			}
		}
	}
}