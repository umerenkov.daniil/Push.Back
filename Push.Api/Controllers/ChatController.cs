﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Push.Api.Controllers.Base;
using Push.Api.Models.Chat;
using Push.Api.Models.Response;
using Push.Api.Models.Users;
using Push.Core.SignalR;
using Push.Core.SignalR.Interfaces;
using ChatService = Push.Api.Services.ChatService;

namespace Push.Api.Controllers
{
	[Route("api/[controller]")]
    public class ChatController : BaseController
    {
	    private readonly ChatService _chatService;

	    public ChatController(ChatService chatService)
	    {
		    _chatService = chatService;
	    }

	    [Authorize]
	    [HttpGet("[action]")]
	    public async Task<Response<List<ChatDto>>> AllChats()
	    {
		    try
		    {
			    var chats = await _chatService.GetAllChats();
			    return Ack(chats);
		    }
		    catch (Exception ex)
		    {
			    return Nack<List<ChatDto>>(ex);
		    }
		}

	    [Authorize]
	    [HttpGet("[action]")]
	    public async Task<Response<List<UserDto>>> AllUsersToStartChat()
	    {
		    try
		    {
			    var users = await _chatService.AllUsersToStartChat();
			    return Ack(users);
		    }
		    catch (Exception ex)
		    {
			    return Nack<List<UserDto>>(ex);
		    }
	    }

		[Authorize]
		[HttpPost("[action]")]
		public async Task<Response<object>> StartChat([FromBody] Guid targetUserId)
		{
			try
			{
				await _chatService.StartChat(targetUserId);
				return Ack<object>(null);
			}
			catch (Exception ex)
			{
				return Nack<object>(ex);
			}
		}

		[Authorize]
	    [HttpGet("[action]")]
	    public async Task<Response<object>> AddConnection([FromQuery] string connectionId)
	    {
		    try
		    {
			    await _chatService.AddToDictionary(connectionId);
			    return Ack<object>(null);
		    }
		    catch (Exception ex)
		    {
			    return Nack<object>(ex);
		    }
	    }

	    [Authorize]
	    [HttpPost("[action]")]
	    public async Task<Response<object>> AddMessage([FromQuery] Guid chatId, [FromBody] MessageDto message)
	    {
			try
			{
				await _chatService.AddMessage(chatId, message);
				return Ack<object>(null);
			}
			catch (Exception ex)
			{
				return Nack<object>(ex);
			}
		}

	    [Authorize]
	    [HttpGet("[action]")]
	    public async Task<Response<List<MessageDto>>> AllMessages([FromQuery] Guid chatId)
	    {
		    try
		    {
			    var messages = await _chatService.AllMessages(chatId);
			    return Ack(messages);
		    }
		    catch (Exception ex)
		    {
			    return Nack<List<MessageDto>>(ex);
		    }
	    }
    }
}
