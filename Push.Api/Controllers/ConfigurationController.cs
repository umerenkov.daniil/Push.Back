﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Push.Api.Controllers.Base;
using Push.Api.Models.Response;
using Push.Core.Configurations;

namespace Push.Api.Controllers
{
	[Route("api/[controller]")]
    public class ConfigurationController : BaseController
	{
		private readonly CommonSettings _commonSettings;

		public ConfigurationController(IOptions<CommonSettings> commonSettings)
		{
			_commonSettings = commonSettings.Value;
		}

		[AllowAnonymous]
		[HttpGet("[action]")]
		public Response<CommonSettings> GetCommonSettings()
		{
			return Ack(_commonSettings);
		}
    }
}