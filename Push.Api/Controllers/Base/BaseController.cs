﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Push.Api.Models.Response;
using Push.Api.Models.Response.Error;
using Push.Core.Models.Exceptions;

namespace Push.Api.Controllers.Base
{
    public abstract class BaseController : Controller
	{
		public Response<TData> Ack<TData>(TData model)
		{
			HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
			return new Response<TData>
			{
				Code = ResponseCode.Ok,
				Data = model
			};
		}

		public Response<TData> Nack<TData>(Exception ex)
		{
			HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;

			var error = new ErrorDto();

			if (ex is AppException test)
			{
				error.Type = ErrorType.AppException;
				error.Messages = test.Errors;
			}
			else
			{
				error.Type = ErrorType.UndefinedException;
				error.Messages = new[] { ex.Message };
			}

			return new Response<TData>
			{
				Code = ResponseCode.Ok,
				Data = default(TData),
				Error = error
			};
		}
	}
}
