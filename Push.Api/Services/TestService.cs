﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Push.Api.Services
{
    public class TestService
    {
	    public readonly Core.Services.TestService _testService;

	    public TestService(Core.Services.TestService testService)
	    {
		    _testService = testService;
	    }

	    public DateTime SaveDateTime(DateTime date)
	    {
		    return _testService.SaveDateTime(date);
	    }

	    public DateTime LastInserted()
	    {
		    return _testService.LastInserted();
	    }
    }
}
