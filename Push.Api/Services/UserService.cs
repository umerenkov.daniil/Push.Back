﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Push.Api.Models.Users;
using Push.Domain.Users;

namespace Push.Api.Services
{
    public class UserService
    {
	    private readonly Core.Services.UserService _userService;
	    private readonly IMapper _mapper;

	    public UserService(
		    Core.Services.UserService userService,
		    IMapper mapper)
	    {
		    _userService = userService;
		    _mapper = mapper;
	    }

	    public async Task<bool> Register(RegistrationRequest request)
	    {
		    var user = new User
		    {
			    Email = request.Email,
			    UserName = request.Email
		    };

		    return await _userService.TryRegister(user, request.Password);
	    }

	    public async Task<string> Login(LoginRequest request)
	    {
		    return await _userService.Login(request.Email, request.Password);
	    }

	    public async Task<UserDto> GetCurrentUser()
	    {
		    var user = await _userService.GetCurrentUser();
		    return _mapper.Map<UserDto>(user);
	    }

	    public async Task<List<UserDto>> GetAllUsers()
	    {
		    var users = await _userService.GetAllUsers();
		    return _mapper.Map<List<UserDto>>(users);
	    }
    }
}
