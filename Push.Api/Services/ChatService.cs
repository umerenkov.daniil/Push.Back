﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Push.Api.Models.Chat;
using Push.Api.Models.Users;
using Push.Core.Models.Exceptions;
using Push.Domain.Chat;

namespace Push.Api.Services
{
    public class ChatService
    {
	    private readonly Core.Services.ChatService _chatService;
	    private readonly IMapper _mapper;

	    public ChatService(
		    Core.Services.ChatService chatService,
		    IMapper mapper)
	    {
		    _chatService = chatService;
		    _mapper = mapper;
	    }

	    public async Task<List<ChatDto>> GetAllChats()
	    {
			var chats = await _chatService.GetAllChats();
			return chats.Select(c => new ChatDto
		    {
			    Id = c.Id,
			    Created = c.Created,
			    Users = _mapper.Map<IEnumerable<UserDto>>(c.UserChats.Select(x => x.User))
		    }).ToList();
		}

	    public async Task<List<UserDto>> AllUsersToStartChat()
	    {
		    var users = await _chatService.AllUsersToStartChat();
		    return _mapper.Map<List<UserDto>>(users);
	    }

		public async Task AddToDictionary(string connectionId)
	    {
		    if (string.IsNullOrEmpty(connectionId)) return;

			await _chatService.AddToDictionary(connectionId);
	    }

	    public async Task StartChat(Guid targetUserId)
	    {
		    await _chatService.StartChat(targetUserId);
	    }

	    public async Task AddMessage(Guid chatId, MessageDto messageRequest)
	    {
			if (chatId == Guid.Empty) throw new AppException("ChatId is empty");

		    var message = new Message
		    {
			    ChatId = chatId,
			    Text = messageRequest.Text
		    };

		    await _chatService.AddMessage(message);
	    }

	    public async Task<List<MessageDto>> AllMessages(Guid chatId)
	    {
			if (chatId == Guid.Empty) throw new AppException("Chat id is empty");

		    var messages = await _chatService.AllMessages(chatId);
		    return _mapper.Map<List<MessageDto>>(messages);
	    }
	}
}