﻿using System;
using Push.Api.Models;
using Push.Core.Models.Exceptions;
using Push.Domain.Push;

namespace Push.Api.Services
{
    public class PushService
    {
	    private readonly Core.Services.PushService _pushService;

	    public PushService(Core.Services.PushService pushService)
	    {
		    _pushService = pushService;
	    }

	    public void SaveEndpoint(PushSubscriptionDto endpoint)
	    {
			if (endpoint == null) throw new AppException("No object");
			if (string.IsNullOrWhiteSpace(endpoint.Endpoint)) throw new AppException("No valid endpoint");
			if (endpoint.Keys == null) throw new AppException("No keys");
			if (string.IsNullOrWhiteSpace(endpoint.Keys.Auth)) throw new AppException("No auth");
			if (string.IsNullOrWhiteSpace(endpoint.Keys.Auth)) throw new AppException("No p256dh");

			var model = new PushEndpoint
		    {
			    Active = true,
			    Auth = endpoint.Keys.Auth,
			    Endpoint = endpoint.Endpoint,
			    Id = new Guid(),
			    P256Dh = endpoint.Keys.P256Dh
		    };

		    _pushService.SaveEndpoint(model).Wait();
	    }
    }
}
