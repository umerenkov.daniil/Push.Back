﻿namespace Push.Api.Models
{
    public class PushSubscriptionDto
    {
	    public string Endpoint { get; set; }
	    public PushSubscriptionKeys Keys { get; set; }
    }

	public class PushSubscriptionKeys
	{
		public string Auth { get; set; }
		public string P256Dh { get; set; }
	}
}
