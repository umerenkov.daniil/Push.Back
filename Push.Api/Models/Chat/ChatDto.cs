﻿using System;
using System.Collections.Generic;
using Push.Api.Models.Users;

namespace Push.Api.Models.Chat
{
    public class ChatDto
    {
	    public Guid Id { get; set; }
	    public DateTime Created { get; set; }
	    public IEnumerable<UserDto> Users { get; set; }
	}
}