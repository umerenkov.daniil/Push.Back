﻿using System;

namespace Push.Api.Models.Chat
{
    public class MessageDto
    {
	    public Guid? Id { get; set; }
	    public Guid? UserId { get; set; }
	    public string Text { get; set; }
	    public DateTime? Created { get; set; }
    }
}
