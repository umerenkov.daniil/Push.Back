﻿namespace Push.Api.Models.Users
{
    public class RegistrationRequest
    {
	    public string Email { get; set; }
	    public string Password { get; set; }
    }
}
