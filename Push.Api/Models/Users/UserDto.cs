﻿using System;

namespace Push.Api.Models.Users
{
    public class UserDto
    {
	    public Guid Id { get; set; }
	    public string Email { get; set; }
    }
}