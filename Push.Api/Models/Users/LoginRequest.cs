﻿namespace Push.Api.Models.Users
{
    public class LoginRequest
    {
	    public string Email { get; set; }
	    public string Password { get; set; }
    }
}
