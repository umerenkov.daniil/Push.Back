﻿using Push.Api.Models.Response.Error;

namespace Push.Api.Models.Response
{
    public class Response<TData>
    {
	    public ResponseCode Code { get; set; }
	    public ErrorDto Error { get; set; }
	    public TData Data { get; set; }
	}
}
