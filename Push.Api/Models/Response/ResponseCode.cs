﻿namespace Push.Api.Models.Response
{
    public enum ResponseCode
    {
	    Ok = 0,
	    Error = 1
	}
}
