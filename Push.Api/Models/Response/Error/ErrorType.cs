﻿namespace Push.Api.Models.Response.Error
{
    public enum ErrorType
    {
	    AppException = 0,
	    UndefinedException = 1
	}
}
