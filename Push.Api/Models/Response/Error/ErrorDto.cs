﻿using System.Collections.Generic;

namespace Push.Api.Models.Response.Error
{
    public class ErrorDto
    {
	    public ErrorType Type { get; set; }
	    public IEnumerable<string> Messages { get; set; }
	}
}
