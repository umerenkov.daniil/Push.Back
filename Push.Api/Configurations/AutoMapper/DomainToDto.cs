﻿using AutoMapper;
using Push.Api.Models.Chat;
using Push.Api.Models.Users;
using Push.Domain.Chat;
using Push.Domain.Users;

namespace Push.Api.Configurations.AutoMapper
{
	public class DomainToDto : Profile
	{
		public DomainToDto()
		{
			CreateMap<User, UserDto>();
			CreateMap<Message, MessageDto>();
		}
	}
}
