﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Push.Core.Configurations;
using Push.Domain.Users;

namespace Push.Core.EF
{
	public class DbAccessor<TContext> where TContext : IdentityDbContext<User, Role, Guid>
	{
		private readonly IServiceProvider _serviceProvider;
		private readonly IOptions<DbConfiguration> _dbConfiguration;

		public DbAccessor(
			IOptions<DbConfiguration> dbConfiguration,
			IServiceProvider serviceProvider)
		{
			_dbConfiguration = dbConfiguration;
			_serviceProvider = serviceProvider;
		}

		public TContext Create(bool disableChangeTracking = false)
		{
			var optionsBuilder = new DbContextOptionsBuilder<TContext>();
			optionsBuilder.UseSqlServer(_dbConfiguration.Value.DefaultConnection);

			if (!(Activator.CreateInstance(typeof(TContext), optionsBuilder.Options) is TContext dbContext))
			{
				throw new Exception($"Не удалось создать {typeof(TContext).FullName}");
			}

			if (disableChangeTracking)
			{
				dbContext.ChangeTracker.AutoDetectChangesEnabled = false;
				dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
			}

			return dbContext;
		}
	}
}
