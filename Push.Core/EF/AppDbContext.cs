﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Push.Domain.Chat;
using Push.Domain.Push;
using Push.Domain.Test;
using Push.Domain.Users;

namespace Push.Core.EF
{
    public class AppDbContext : IdentityDbContext<User, Role, Guid>
    {
	    public DbSet<Chat> Chats { get; set; }
	    public DbSet<Message> Messages { get; set; }
	    public DbSet<UserChat> UserChats { get; set; }
	    public DbSet<PushEndpoint> PushEndpoints { get; set; }
	    public DbSet<Test> Tests { get; set; }

		public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

	    protected override void OnModelCreating(ModelBuilder builder)
	    {
		    builder.Entity<Role>().HasData(new Role
		    {
				Id = Guid.NewGuid(),
				Name = "Administator",
				NormalizedName = "Administator"
			});

		    builder.Entity<Role>().HasData(new Role
		    {
			    Id = Guid.NewGuid(),
			    Name = "User",
				NormalizedName = "User"
			});

		    builder.Entity<UserChat>()
			    .HasKey(uc => new {uc.ChatId, uc.UserId});

		    builder.Entity<UserChat>()
			    .HasOne(pc => pc.User)
			    .WithMany(p => p.UserChats)
			    .HasForeignKey(pc => pc.UserId);

		    builder.Entity<UserChat>()
			    .HasOne(pc => pc.Chat)
			    .WithMany(p => p.UserChats)
			    .HasForeignKey(pc => pc.ChatId);

			base.OnModelCreating(builder);
	    }
    }
}