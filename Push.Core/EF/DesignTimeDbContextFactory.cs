﻿using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Push.Core.EF
{
	public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
	{
		public AppDbContext CreateDbContext(string[] args)
		{
			var path = Directory.GetParent(Directory.GetCurrentDirectory());
			var dirs = path.GetDirectories();

			var target = dirs.DefaultIfEmpty().FirstOrDefault(x => x.Name.Contains("Push.Api"));

			IConfigurationRoot configuration = new ConfigurationBuilder()
				.SetBasePath(target.FullName)
				.AddJsonFile("appsettings.json")
				.AddEnvironmentVariables()
				.Build();

			var builder = new DbContextOptionsBuilder<AppDbContext>();

			var connectionString = configuration.GetConnectionString("DefaultConnection");

			builder.UseSqlServer(connectionString);

			return new AppDbContext(builder.Options);
		}
	}
}
