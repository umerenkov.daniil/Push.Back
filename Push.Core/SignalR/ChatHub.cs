﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Push.Core.SignalR.Interfaces;
using ConnectionInfo = Push.Core.Models.SignalR.ConnectionInfo;

namespace Push.Core.SignalR
{
    public class ChatHub : Hub<IChatHub>
    {
	    public async Task<ConnectionInfo> GetConnectionInfo()
	    {
		    return new ConnectionInfo
		    {
				ConnectionId = Context.ConnectionId
		    };
	    }
		
	    public override Task OnDisconnectedAsync(Exception exception)
	    {
		    try
		    {
			    var connections =
				    ChatConnections.UserChatConnections.FirstOrDefault(x =>
					    x.Value.Any(v => v == Context.ConnectionId));

			    connections.Value?.Remove(Context.ConnectionId);
		    }
		    catch (Exception ex)
		    {
			    // ignored
		    }

			return base.OnDisconnectedAsync(exception);
	    }
    }
}
