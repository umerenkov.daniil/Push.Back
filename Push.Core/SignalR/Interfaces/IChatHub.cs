﻿using System.Threading.Tasks;
using Push.Core.Models.SignalR;

namespace Push.Core.SignalR.Interfaces
{
    public interface IChatHub
    {
	    Task ChatMessage(ChatMessage message);
    }
}
