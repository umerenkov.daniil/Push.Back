﻿namespace Push.Core.Configurations
{
    public class CommonSettings
    {
	    public string ApiBase { get; set; }
	    public string ChatUrl { get; set; }
	}
}