﻿using System;
using Microsoft.IdentityModel.Tokens;

namespace Push.Core.Configurations
{
    public class JwtSettings
    {
	    public string SecretKey { get; set; }
	    public string Issuer { get; set; }
	    public string Audience { get; set; }

	    public SigningCredentials SigningCredentials { get; set; }

		public TimeSpan TokenLifeSpan { get; set; }
	}
}
