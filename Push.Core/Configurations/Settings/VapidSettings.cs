﻿namespace Push.Core.Configurations.Settings
{
    public class VapidSettings
    {
	    public string Public { get; set; }
	    public string Private { get; set; }
	    public string Link { get; set; }
    }
}
