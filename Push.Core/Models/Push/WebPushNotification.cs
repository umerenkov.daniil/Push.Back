﻿using Newtonsoft.Json;

namespace Push.Core.Models.Push
{
    public class WebPushNotificationWrapper
    {
		[JsonProperty("notification")]
	    public WebPushNotification Notification { get; set; }
    }

	public class WebPushNotification
	{
		[JsonProperty("title")]
		public string Title { get; set; }

		[JsonProperty("body")]
		public string Body { get; set; }

		[JsonProperty("icon")]
		public string Icon { get; set; }

		[JsonProperty("image")]
		public string Image { get; set; }

		[JsonProperty("data")]
		public WebPushData Data { get; set; }
	}

	public class WebPushData
	{
		[JsonProperty("url")]
		public string Url { get; set; }
	}
}
