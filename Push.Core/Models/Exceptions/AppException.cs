﻿using System;
using System.Collections.Generic;

namespace Push.Core.Models.Exceptions
{
    public class AppException : Exception
    {
	    public IEnumerable<string> Errors { get; set; }

	    public AppException(string error) : base(error)
	    {
		    Errors = new[] { error };
	    }

	    public AppException(IEnumerable<string> errors) : base(string.Join(" ", errors))
	    {
		    Errors = errors;
	    }
	}
}
