﻿using System;

namespace Push.Core.Models.SignalR
{
    public class ChatMessage
    {
	    public Guid ChatId { get; set; }
		public Guid UserId { get; set; }
		public string Text { get; set; }
    }
}
