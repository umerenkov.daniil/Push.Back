﻿namespace Push.Core.Models.SignalR
{
    public class ConnectionInfo
    {
	    public string ConnectionId { get; set; }
    }
}
