﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Push.Core.EF;
using Push.Core.Models.Exceptions;
using Push.Core.Models.SignalR;
using Push.Core.SignalR;
using Push.Core.SignalR.Interfaces;
using Push.Domain.Chat;
using Push.Domain.Users;

namespace Push.Core.Services
{
    public class ChatService
    {
	    private readonly UserService _userService;
	    private readonly ConcurrentDictionary<Guid, List<string>> _connections;
	    private readonly DbAccessor<AppDbContext> _accessor;
	    private readonly IHubContext<ChatHub, IChatHub> _chatHub;
	    private readonly PushService _pushService;

		public ChatService(
			UserService userService,
			DbAccessor<AppDbContext> accessor,
			PushService pushService,
			IHubContext<ChatHub, IChatHub> chatHub)
	    {
		    _userService = userService;
		    _connections = ChatConnections.UserChatConnections;
		    _accessor = accessor;
		    _chatHub = chatHub;
		    _pushService = pushService;
	    }

	    public async Task<List<Chat>> GetAllChats()
	    {
		    var user = await _userService.GetCurrentUser();
			if (user == null) throw new AppException("No user");

		    using (var context = _accessor.Create())
		    {
				return await context.Chats
					.Where(c => context.UserChats.Where(x => x.UserId == user.Id).Select(x => x.ChatId).Contains(c.Id))
					.Include(x => x.UserChats).ThenInclude(x => x.User)
					.ToListAsync();
			}
	    }

	    public async Task<List<User>> AllUsersToStartChat()
	    {
		    var user = await _userService.GetCurrentUser();
		    if (user == null) throw new AppException("No user");

		    using (var context = _accessor.Create())
		    {
			    var existingChats = context.UserChats.Where(x => x.UserId == user.Id).Select(x => x.ChatId);
			    var userIntersection =
				    context.UserChats.Where(x => existingChats.Contains(x.ChatId)).Select(x => x.UserId);

				return await context.Users.Where(x => !userIntersection.Contains(x.Id)).ToListAsync();
			}
	    }
		
		public async Task AddToDictionary(string connectionId)
		{
			User user;
			using (var context = _accessor.Create())
			{
				user = await _userService.TryGetUser(context);
			}
		    
			if (user == null) throw new AppException("User not found");
			var hasAnyConnection = _connections.TryGetValue(user.Id, out var userConnections);

			if (hasAnyConnection)
			{
				if (userConnections.All(x => x != connectionId)) userConnections.Add(connectionId);
			}
			else
			{
				if (!_connections.TryAdd(user.Id, new List<string> {connectionId})) throw new AppException("Error adding to connections");
			}
		}

	    public async Task StartChat(Guid targetUserId)
	    {
		    using (var context = _accessor.Create())
		    {
			    var user = await _userService.TryGetUser(context);
			    if (user == null) throw new AppException("User not found");

				var targetUser = await context.Users.AsNoTracking().SingleOrDefaultAsync(u => u.Id == targetUserId);
			    if (targetUser == null) throw new AppException("Target user not found");

			    var chatId = Guid.NewGuid();
				var chat = new Chat
				{
					Id = chatId,
					Created = DateTime.Now,
					UserChats = new Collection<UserChat>
					{
						new UserChat
						{
							UserId = user.Id,
							ChatId = chatId
						},
						new UserChat
						{
							UserId = targetUserId,
							ChatId = chatId
						}
					}
				};

				await context.Chats.AddAsync(chat);
				await context.SaveChangesAsync();
			}
		}

	    public async Task AddMessage(Message message)
	    {
		    using (var context = _accessor.Create())
		    {
			    var targetChat = await context.Chats.AsNoTracking()
				    .Include(x => x.UserChats).ThenInclude(x => x.User)
				    .SingleOrDefaultAsync(x => x.Id == message.ChatId);

				if (targetChat == null) throw new AppException("Chat not found");

			    var user = await _userService.TryGetUser(context);
				if (user == null) throw new AppException("Current user error");

				message.Id = new Guid();
			    message.UserId = user.Id;
				message.Created = DateTime.Now;

			    await context.Messages.AddAsync(message);
			    await context.SaveChangesAsync();

			    await ChatNotify(targetChat, user, message, context);
		    }
	    }

	    public async Task ChatNotify(Chat chat, User fromUser, Message message, AppDbContext context)
	    {
		    var toNotify = chat.UserChats.Select(x => x.User).Where(x => x.Id != fromUser.Id).ToList();

		    foreach (var user in toNotify)
		    {
			    var hasConnections = _connections.TryGetValue(user.Id, out var connections);
			    if (hasConnections && connections.Count > 0)
			    {
				    connections.ForEach(connection =>
				    {
					    var targetClient = _chatHub.Clients.Client(connection);
					    targetClient.ChatMessage(new ChatMessage
					    {
						    ChatId = chat.Id,
						    UserId = fromUser.Id,
						    Text = message.Text
					    });
				    });
			    }
			    else await _pushService.NotifyNewMessage(message, user, context);
			}
	    }

	    public async Task<List<Message>> AllMessages(Guid chatId)
	    {
		    using (var context = _accessor.Create())
		    {
			    return await context.Messages.Where(x => x.ChatId == chatId).OrderBy(x => x.Created).ToListAsync();
		    }
	    }
	}
}
