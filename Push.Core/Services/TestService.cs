﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Push.Core.EF;
using Push.Core.Models.Exceptions;
using Push.Domain.Test;

namespace Push.Core.Services
{
    public class TestService
    {
	    private readonly DbAccessor<AppDbContext> _accessor;

	    public TestService(DbAccessor<AppDbContext> accessor)
	    {
		    _accessor = accessor;
	    }

	    public DateTime SaveDateTime(DateTime date)
	    {
		    using (var context = _accessor.Create())
		    {
			    var test = new Test
			    {
				    DateTime = date
			    };

				context.Tests.Add(test);
			    context.SaveChanges();

			    test = context.Tests.SingleOrDefault(x => x.Id == test.Id);
				if (test == null) throw new AppException("No data");
			    return test.DateTime;
		    }
	    }

	    public DateTime LastInserted()
	    {
		    using (var context = _accessor.Create())
		    {
			    var test = context.Tests.LastOrDefault();
			    if (test == null) throw new AppException("No data");
			    return test.DateTime;
		    }
		}
	}
}
