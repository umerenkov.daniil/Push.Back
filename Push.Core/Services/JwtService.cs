﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Push.Core.Configurations;
using Push.Domain.Users;

namespace Push.Core.Services
{
    public class JwtService
    {
	    private readonly JwtSettings _jwtSettings;

	    public JwtService(IOptions<JwtSettings> jwtSettings)
	    {
		    _jwtSettings = jwtSettings.Value;
	    }

	    public string CreateToken(User user, Role role)
	    {
		    var tokenHandler = new JwtSecurityTokenHandler();
		    var key = Encoding.ASCII.GetBytes(_jwtSettings.SecretKey);
		    var tokenDescriptor = new SecurityTokenDescriptor
		    {
			    Subject = new ClaimsIdentity(new []
			    {
				    new Claim(ClaimTypes.Name, user.Id.ToString())
			    }),
			    Expires = DateTime.UtcNow.AddDays(7),
			    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
		    };
		    var token = tokenHandler.CreateToken(tokenDescriptor);
		    return tokenHandler.WriteToken(token);
	    }
	}
}
