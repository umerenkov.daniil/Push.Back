﻿using Microsoft.Extensions.Options;
using Push.Core.Configurations.Settings;
using WebPush;

namespace Push.Core.Services
{
	public class WebPushService : WebPushClient
	{
		private readonly VapidSettings _vapidSettings;

		public WebPushService(IOptions<VapidSettings> vapidSettings)
		{
			_vapidSettings = vapidSettings.Value;
			SetVapidDetails(_vapidSettings.Link, _vapidSettings.Public, _vapidSettings.Private);
		}
	}
}
