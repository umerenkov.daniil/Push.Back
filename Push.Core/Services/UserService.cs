﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Push.Core.EF;
using Push.Core.Models.Exceptions;
using Push.Domain.Users;

namespace Push.Core.Services
{
    public class UserService
    {
	    private readonly UserManager<User> _userManager;
	    private readonly RoleManager<Role> _roleManager;
	    private readonly JwtService _jwtService;
	    private readonly ClaimsPrincipal _userClaims;
	    private readonly DbAccessor<AppDbContext> _accessor;

		public UserService(
			UserManager<User> userManager,
			JwtService jwtService,
			RoleManager<Role> roleManager,
			IHttpContextAccessor httpContextAccessor,
			DbAccessor<AppDbContext> accessor)
	    {
		    _userManager = userManager;
		    _jwtService = jwtService;
		    _roleManager = roleManager;
		    _userClaims = httpContextAccessor.HttpContext.User;
		    _accessor = accessor;
	    }

	    public async Task<bool> TryRegister(User user, string password)
	    {
		    var created = await _userManager.CreateAsync(user, password);
		    if (created.Succeeded) await _userManager.AddToRoleAsync(user, "User");
		    return created.Succeeded;
	    }

	    public async Task<string> Login(string email, string password)
	    {
		    var user = await _userManager.FindByEmailAsync(email);
			if (user == null) throw new AppException("No user with this login");
			
			if (!await _userManager.CheckPasswordAsync(user, password)) throw new AppException("Invalid password");

		    var userRoles = await _userManager.GetRolesAsync(user);
		    var userRole = userRoles.FirstOrDefault();

			if (userRole == null) throw new AppException("No roles");

		    var role = await _roleManager.FindByNameAsync(userRole);

			return _jwtService.CreateToken(user, role);
	    }

	    public async Task<User> GetCurrentUser()
	    {
		    using (var context = _accessor.Create())
		    {
			    var user = await TryGetUser(context);
			    if (user == null) throw new AppException("User not found");
			    return user;
			}
	    }

	    public async Task<User> TryGetUser(AppDbContext context)
	    {
		    var idClaim = _userClaims.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name);
		    if (idClaim == null || string.IsNullOrWhiteSpace(idClaim.Value)) return null;

			return await context.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Id == new Guid(idClaim.Value));
		}

	    public async Task<List<User>> GetAllUsers()
	    {
		    using (var context = _accessor.Create())
		    {
			    return await context.Users.ToListAsync();
		    }
	    }
    }
}
