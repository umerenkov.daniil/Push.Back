﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Push.Core.EF;
using Push.Core.Models.Exceptions;
using Push.Core.Models.Push;
using Push.Domain.Chat;
using Push.Domain.Push;
using Push.Domain.Users;
using WebPush;

namespace Push.Core.Services
{
    public class PushService
    {
	    private readonly DbAccessor<AppDbContext> _accessor;
	    private readonly UserService _userService;
	    private readonly WebPushService _webPushService;

	    public PushService(
		    DbAccessor<AppDbContext> accessor,
			WebPushService webPushService,
		    UserService userService)
	    {
		    _accessor = accessor;
		    _userService = userService;
		    _webPushService = webPushService;
	    }

	    public async Task SaveEndpoint(PushEndpoint endpoint)
	    {
		    using (var context = _accessor.Create())
		    {
			    var user = await _userService.TryGetUser(context);
				if (user == null) throw new AppException("no user");

			    endpoint.UserId = user.Id;

			    var saveEndpoint = await context.PushEndpoints.SingleOrDefaultAsync(e =>
				    e.P256Dh == endpoint.P256Dh && e.Auth == endpoint.Auth && e.Endpoint == endpoint.Endpoint && e.UserId == endpoint.UserId);

			    if (saveEndpoint != null) return;

				await context.PushEndpoints.AddAsync(endpoint);
			    await context.SaveChangesAsync();
		    }
	    }

	    public async Task NotifyNewMessage(Message message, User user, AppDbContext context)
	    {
		    var endpoints = context.PushEndpoints.Where(x => x.UserId == user.Id && x.Active).ToList();

		    foreach (var endpoint in endpoints)
		    {
			    var subscription = new PushSubscription(endpoint.Endpoint, endpoint.P256Dh, endpoint.Auth);
			    var notification = new WebPushNotificationWrapper
			    {
				    Notification = new WebPushNotification
				    {
					    Title = "Новое сообщение test",
					    Body = $"{message.Text}",
						Icon = "http://assets-cf.gbeye.com/thumbnails/frame_box_full_frame_99277_1456795432.jpg",
						Data = new WebPushData
						{
							Url = "https://app.tenderhub.ru"
						}
					}
			    };

			    var stringifiedNotification = JsonConvert.SerializeObject(notification);

			    try
			    {
					await _webPushService.SendNotificationAsync(subscription, stringifiedNotification);
			    }
			    catch (Exception exception)
			    {
				    if (exception.InnerException is WebPushException) endpoint.Active = false;
				}
		    }

		    await context.SaveChangesAsync();
	    }
    }
}
