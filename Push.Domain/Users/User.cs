﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Push.Domain.Chat;

namespace Push.Domain.Users
{
    public class User : IdentityUser<Guid>
    {
	    public virtual IEnumerable<UserChat> UserChats { get; set; }
	}
}
