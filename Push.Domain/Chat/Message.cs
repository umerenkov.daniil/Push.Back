﻿using System;
using System.Collections.Generic;
using System.Text;
using Push.Domain.Users;

namespace Push.Domain.Chat
{
    public class Message
    {
	    public Guid Id { get; set; }

	    public Guid UserId { get; set; }
		public virtual User User { get; set; }

	    public string Text { get; set; }
	    public DateTime Created { get; set; }

	    public Guid ChatId { get; set; }
	    public virtual Chat Chat { get; set; }
	}
}