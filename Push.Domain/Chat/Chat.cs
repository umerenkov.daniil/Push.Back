﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Push.Domain.Chat
{
    public class Chat
    {
	    public Guid Id { get; set; }
	    public DateTime Created { get; set; }
		public virtual ICollection<UserChat> UserChats { get; set; }
    }
}
