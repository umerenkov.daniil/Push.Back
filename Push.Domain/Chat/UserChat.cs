﻿using System;
using System.Collections.Generic;
using System.Text;
using Push.Domain.Users;

namespace Push.Domain.Chat
{
    public class UserChat
    {
	    public Guid UserId { get; set; }
	    public virtual User User { get; set; }
	    public Guid ChatId { get; set; }
	    public virtual Chat Chat { get; set; }
    }
}
