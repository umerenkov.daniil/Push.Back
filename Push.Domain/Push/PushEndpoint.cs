﻿using System;
using System.Collections.Generic;
using System.Text;
using Push.Domain.Users;

namespace Push.Domain.Push
{
    public class PushEndpoint
    {
	    public Guid Id { get; set; }
	    public string Endpoint { get; set; }
	    public string Auth { get; set; }
	    public string P256Dh { get; set; }
	    public bool Active { get; set; }

		public Guid UserId { get; set; }
		public virtual User User { get; set; }
    }
}
